export XDG_DATA_HOME="${HOME}/.local/share"
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_STATE_HOME="${HOME}/.local/state"
export XDG_RUNTIME_DIR="/run/user/${UID}"
export XDG_DATA_DIRS='/usr/local/share:/usr/share'
export XDG_CONFIG_DIRS='/etc/xdg'

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

setopt inc_append_history

autoload compinit
compinit

[[ -f "${HOME}/.profile" ]] && source "${HOME}/.profile"

source "${ZDOTDIR}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

if [ -d "$ZDOTDIR/zsh-autosuggestions" ]; then
    source $ZDOTDIR/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
    bindkey '^ ' autosuggest-accept
    export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=#CE1A5C,bold'
fi

source "${ZDOTDIR}/powerlevel10k/powerlevel10k.zsh-theme"
[[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh

export HISTFILE="${XDG_CACHE_HOME}/zsh-history"
export SAVEHIST=1000

function cmd() { command -v "${1}" 2>&1 >/dev/null; }

export PATH="${PATH}:${HOME}/.local/bin"
export LIBRARY_PATH="${HOME}/.local/lib:${LIBRARY_PATH}"
export LD_LIBRARY_PATH="${LIBRARY_PATH}"
export INCLUDE_PATH="${HOME}/.local/include:${INCLUDE_PATH}"
export C_INCLUDE_PATH="${INCLUDE_PATH}"
export PKG_CONFIG_PATH="${HOME}/.local/lib/pkgconfig:${PKG_CONFIG_PATH}"

cmd 'zoxide' && eval "$(zoxide init zsh)"
cmd 'eza' && alias ls="eza"
cmd 'bat' && alias cat="bat"

cmd 'gpg' && export GNUPGHOME="${XDG_DATA_HOME}/gnupg"
cmd 'cargo' && export CARGO_HOME="${XDG_DATA_HOME}/cargo"
cmd 'rustup' && export RUSTUP_HOME="${XDG_DATA_HOME}/rustup"

if cmd 'pass'; then
  export PASSWORD_STORE_DIR="${XDG_DATA_HOME}/pass"
  alias ssh='pass show -c ssh/soz && ssh'
  alias scp='pass show -c ssh/soz && scp -r'
  alias sftp='pass show -c ssh/soz && sftp'
  alias pg='pass show -c git/codeberg/seigakaku -f token'
  alias pss='pass show -c ssh/soz'
fi

if cmd 'X' || cmd 'Xorg' || cmd 'X11'; then
  export XINITRC="${XDG_CONFIG_HOME}/X11/xinitrc"
  export XSERVERRC="${XDG_CONFIG_HOME}/X11/xserverrc"
  export XAUTHORITY="${XDG_RUNTIME_DIR}/Xauthority"
fi

export GTK_THEME='Adwaita:dark'
export QT_STYLE_OVERRIDE='Adwaita-Dark'

if cmd 'nvim'; then
  alias vi='nvim'
  alias vim='nvim'
  export EDITOR='nvim'
  export VISUAL="${EDITOR}"
fi

if cmd 'sbcl'; then
  if cmd 'rlwrap'; then
    alias sbcl='rlwrap sbcl'
  fi
  function slynk () {
    sbcl --eval "(ql:quickload :slynk)" \
      --eval "(slynk:create-server :port $1 :dont-close t)"
  }
fi


if cmd 'adb'; then
  export TERMUX='/data/data/com.termux/files'
fi

if [[ $- == *i* ]]; then
  if [[ "$(hostname)" == 'ideapad' ]]; then
    if [[ $(tty) =~ /dev/tty* ]] && cmd 'yaft'; then
      yaft tmux
    fi
  fi
fi
